-- Write a query to display: 
-- 1. the first name, last name, department number, and department name for each employee.
SELECT FIRST_NAME, LAST_NAME, DEPARTMENT_ID, DEPARTMENT_NAME
FROM EMPLOYEES E LEFT JOIN DEPARTMENTS D USING(DEPARTMENT_ID);

-- 2. the first and last name, department, city, and state province for each employee.
SELECT E.FIRST_NAME, E.LAST_NAME,E.DEPARTMENT_ID,
       L.CITY,L.STATE_PROVINCE,
       D.DEPARTMENT_ID, D.DEPARTMENT_NAME
  FROM DEPARTMENTS D,EMPLOYEES E,LOCATIONS L
       WHERE D.DEPARTMENT_ID = E.DEPARTMENT_ID
       ORDER BY E.FIRST_NAME;
-- 3. the first name, last name, salary, and job grade for all employees.
SELECT E.FIRST_NAME, E.LAST_NAME, E.SALARY, J.JOB_TITLE
       FROM EMPLOYEES E, JOBS J
       WHERE E.JOB_ID = J.JOB_ID
       ORDER BY E.LAST_NAME;
-- 4. the first name, last name, department number and department name, for all employees for departments 80 or 40.
SELECT FIRST_NAME, LAST_NAME, DEPARTMENT_ID, DEPARTMENT_NAME
       FROM EMPLOYEES E JOIN DEPARTMENTS D USING(DEPARTMENT_ID)
         WHERE DEPARTMENT_ID=40 OR DEPARTMENT_ID=80;
-- 5. those employees who contain a letter z to their first name and also display their last name, department, city, and state province.
SELECT FIRST_NAME, LAST_NAME, DEPARTMENT_NAME,CITY, STATE_PROVINCE
       FROM EMPLOYEES E JOIN DEPARTMENTS D USING (DEPARTMENT_ID)
                        JOIN LOCATIONS L USING(LOCATION_ID)
       WHERE FIRST_NAME LIKE '%z%' OR FIRST_NAME LIKE 'z%' OR FIRST_NAME LIKE '%z';
-- 6. all departments including those where does not have any employee.
SELECT DEPARTMENT_ID, DEPARTMENT_NAME
      FROM DEPARTMENTS;
      
-- 7. the first and last name and salary for those employees who earn less than the employee earn whose number is 182.
SELECT FIRST_NAME, LAST_NAME, SALARY
       FROM EMPLOYEES
       WHERE SALARY < (SELECT SALARY FROM EMPLOYEES WHERE EMPLOYEE_ID = 182);
-- 8. the first name of all employees including the first name of their manager.
      SELECT e.FIRST_NAME, m.first_name AS MANAGER_NAME
         FROM EMPLOYEES E JOIN EMPLOYEES M ON(E.MANAGER_ID = M.EMPLOYEE_ID);
                                         
-- 9. the department name, city, and state province for each department.
SELECT DEPARTMENT_NAME, CITY, STATE_PROVINCE
       FROM DEPARTMENTS D JOIN LOCATIONS L USING(LOCATION_ID);
--10. the first name, last name, department number and name, for all employees who have or have not any department.
SELECT FIRST_NAME, LAST_NAME, DEPARTMENT_ID, DEPARTMENT_NAME
       FROM EMPLOYEES E JOIN DEPARTMENTS USING(DEPARTMENT_ID)
       WHERE DEPARTMENT_NAME IS NULL OR DEPARTMENT_NAME IS NOT NULL;
      
--11. the first name of all employees and the first name of their manager including those who does not working under any manager.
SELECT E.FIRST_NAME AS EMPLOYEE_NAME, M.FIRST_NAME AS MANAGER_NAME
       FROM EMPLOYEES E
       LEFT JOIN EMPLOYEES M ON( E.MANAGER_ID = M.EMPLOYEE_ID)
       WHERE( M.EMPLOYEE_ID IS NULL OR  M.EMPLOYEE_ID IS NOT NULL )
       ORDER BY E.FIRST_NAME;
      
--12. the first name, last name, and department number for those employees who works in the same department as the employee who holds the last name as Taylor.
       SELECT FIRST_NAME, LAST_NAME, DEPARTMENT_ID
       FROM EMPLOYEES
       WHERE DEPARTMENT_ID in (SELECT department_id from employees where last_name = 'Taylor')
       ORDER BY FIRST_NAME;
--13. the job title, department name, full name (first and last name ) of employee, and starting date for all the jobs which started on or after 1st January, 1993 and ending with on or before 31 August, 1997

SELECT J.JOB_TITLE, D.DEPARTMENT_NAME, E.FIRST_NAME, E.LAST_NAME,JH.START_DATE
       FROM JOB_HISTORY JH
            left JOIN employees e ON (jh.EMPLOYEE_ID = e.EMPLOYEE_ID)
            left JOIN DEPARTMENTS D ON(JH.DEPARTMENT_ID = D.DEPARTMENT_ID)
            left JOIN JOBS J ON (JH.JOB_ID = J.JOB_ID)
                  WHERE JH.START_DATE BETWEEN (date'1993-04-12') AND (date'1997-08-31');

--14. job title, full name (first and last name ) of employee, and the difference between maximum salary for the job and salary of the employee.
SELECT JOB_TITLE, FIRST_NAME, LAST_NAME,(MAX_SALARY-SALARY) AS AMPLITUDE_OF_SALARY
       FROM EMPLOYEES E
       JOIN JOBS J ON E.JOB_ID = J.JOB_ID ;
       
--15. the name of the department, average salary and number of employees working in that department who got commission.
select department_name, AVG( E.salary ) ,COUNT(commission_pct) AS NUMBER_OF_EMP
from departments d join employees E using(department_id)
     group by department_name;

--16. the full name (first and last name ) of employee, and job title of those employees who is working in the department which ID is 80.
SELECT E.FIRST_NAME, E.LAST_NAME,J.JOB_TITLE
       FROM EMPLOYEES E FULL JOIN DEPARTMENTS D ON(E.DEPARTMENT_ID = D.DEPARTMENT_ID AND D.MANAGER_ID = E.EMPLOYEE_ID)
                        FULL JOIN JOBS J ON(E.JOB_ID = J.JOB_ID)
                        FULL JOIN JOB_HISTORY JH ON( E.EMPLOYEE_ID = JH.EMPLOYEE_ID AND JH.DEPARTMENT_ID = D.DEPARTMENT_ID AND JH.JOB_ID = J.JOB_ID)
                        WHERE(E.DEPARTMENT_ID = 80);

--17. the name of the country, city, and the departments which are running there.

SELECT C.COUNTRY_NAME, L.CITY, D.DEPARTMENT_NAME
    FROM countries C JOIN LOCATIONS L ON(L.COUNTRY_ID = C.COUNTRY_ID)
                     JOIN DEPARTMENTS D ON(D.LOCATION_ID = L.LOCATION_ID)
                     ORDER BY d.department_name;
                     
--18. department name and the full name (first and last name) of the manager.
SELECT D.DEPARTMENT_NAME, E.FIRST_NAME, E.LAST_NAME
        FROM DEPARTMENTS D JOIN EMPLOYEES E ON (d.department_id = E.DEPARTMENT_ID AND D.MANAGER_ID = E.EMPLOYEE_ID)
             ORDER BY LAST_NAME;

--19. job title and average salary of employees.

SELECT J.JOB_TITLE,AVG(E.SALARY)
    FROM EMPLOYEES E JOIN JOBS J ON(J.JOB_ID = E.JOB_ID)
    GROUP BY J.JOB_TITLE;
        
--20. the details of jobs which was done by any of the employees who is presently earning a salary on and above 12000.
SELECT DISTINCT J.JOB_ID,J.JOB_TITLE
    FROM JOBS J JOIN EMPLOYEES E ON (J.JOB_ID = E.JOB_ID)
                WHERE(SALARY > 12000);
--21. the country name, city, and number of those departments where at leaste 2 employees are working.

SELECT country_name,city, COUNT(department_id)
	FROM countries
		JOIN locations USING (country_id)
		JOIN departments USING (location_id)
              WHERE( department_id IN
                     (SELECT department_id
		                FROM employees
	                    GROUP BY department_id
	                    HAVING COUNT(department_id)>=2))
                        GROUP BY country_name,city;
                     
--22. the department name, full name (first and last name) of manager, and their city.

SELECT D.DEPARTMENT_NAME, E.FIRST_NAME, E.LAST_NAME, L.CITY
        FROM DEPARTMENTS D JOIN EMPLOYEES E ON (d.department_id = E.DEPARTMENT_ID AND D.MANAGER_ID = E.EMPLOYEE_ID)
                           JOIN LOCATIONS L ON (D.LOCATION_ID = L.LOCATION_ID)
                           ORDER BY CITY;

--23. the employee ID, job name, number of days worked in for all those jobs in department 80.
SELECT e.EMPLOYEE_ID, j.JOB_TITLE, (JH.END_DATE - JH.START_DATE)as numbers_of_day_
       FROM jobs j JOIN employees e ON (E.JOB_ID = J.JOB_ID)
                   JOIN JOB_HISTORY JH ON (JH.EMPLOYEE_ID = E.EMPLOYEE_ID AND JH.JOB_ID = J.JOB_ID)
                            WHERE(E.DEPARTMENT_ID = 80);
                            
--24. the full name (first and last name), and salary of those employees who working in any department located in London.
SELECT E.FIRST_NAME, E.LAST_NAME, E.SALARY
       FROM EMPLOYEES E JOIN DEPARTMENTS D ON(E.DEPARTMENT_ID = D.DEPARTMENT_ID)
                        JOIN LOCATIONS L ON(D.LOCATION_ID = L.LOCATION_ID)
                             WHERE (L.CITY = 'London');
--25. full name(first and last name), job title, starting and ending date of last jobs for those employees with worked without a commission percentage.
SELECT E.first_name, E.last_name, J.job_title, JH.start_date, JH.end_date
       FROM JOBS J JOIN EMPLOYEES E  ON(E.JOB_ID = J.JOB_ID)
                   JOIN JOB_HISTORY JH ON (JH.JOB_ID = J.JOB_ID)
                   WHERE (E.COMMISSION_PCT IS NULL);

--26. the department name and number of employees in each of the department.
SELECT D.DEPARTMENT_NAME, COUNT(E.EMPLOYEE_ID)
       FROM DEPARTMENTS D JOIN EMPLOYEES E ON (E.DEPARTMENT_ID = D.DEPARTMENT_ID)
       GROUP BY DEPARTMENT_NAME;

--27. the full name (firt and last name ) of employee with ID and name of the country presently where (s)he is working.
SELECT E.FIRST_NAME, E.LAST_NAME, E.EMPLOYEE_ID, C.COUNTRY_NAME
       FROM EMPLOYEES E JOIN DEPARTMENTS D ON (E.DEPARTMENT_ID = D.DEPARTMENT_ID)
                        JOIN LOCATIONS L ON (D.LOCATION_ID = L.LOCATION_ID)
                        JOIN COUNTRIES C ON (L.COUNTRY_ID = C.COUNTRY_ID)
                        ORDER BY LAST_NAME;

--28. the name ( first name and last name ) for those employees who gets more salary than the employee whose ID is 163.
SELECT FIRST_NAME, LAST_NAME
       FROM EMPLOYEES
       WHERE (SALARY > (SELECT SALARY FROM EMPLOYEES WHERE EMPLOYEE_ID = 163));

--29. the name ( first name and last name ), salary, department id, job id for those employees who works in the same designation as the employee works whose id is 169.
SELECT E.FIRST_NAME, E.LAST_NAME, E.SALARY, D.DEPARTMENT_ID, E.JOB_ID
       FROM EMPLOYEES E JOIN DEPARTMENTS D ON (E.DEPARTMENT_ID = D.DEPARTMENT_ID)
                        WHERE( D.DEPARTMENT_ID IN (SELECT D.DEPARTMENT_ID FROM DEPARTMENTS WHERE (E.EMPLOYEE_ID = 169)));

--30. the name ( first name and last name ), salary, department id for those employees who earn such amount of salary which is the smallest salary of any of the departments.

SELECT (E.FIRST_NAME || E.LAST_NAME) AS NAME, E.SALARY , E.DEPARTMENT_ID
       FROM EMPLOYEES e join JOBS J ON (E.JOB_ID = J.JOB_ID)
        WHERE( E.SALARY <= J.MIN_SALARY);

--31. the employee id, employee name (first name and last name ) for all employees who earn more than the average salary. 

SELECT (E.FIRST_NAME || E.LAST_NAME) AS NAME , E.LAST_NAME
       FROM EMPLOYEES E
            WHERE (E.SALARY > (SELECT AVG(E.SALARY) FROM EMPLOYEES E));

--32. the employee name ( first name and last name ), employee id and salary of all employees who report to Payam. 

SELECT FIRST_NAME , LAST_NAME, EMPLOYEE_ID, SALARY
       FROM EMPLOYEES
            WHERE( MANAGER_ID = ( SELECT EMPLOYEE_ID FROM EMPLOYEES WHERE(first_NAME = 'Payam')));

--33. the department number, name ( first name and last name ), job and department name for all employees in the Finance department. 
SELECT d.department_id , e.first_name, e.last_name, j.job_title, d.department_name
       from departments d join employees e on(e.department_id = d.department_id)
                          join jobs j on(j.job_id = e.job_id)
                          where (d.department_name like '%Finance%' or d.department_name like 'Finance%' or d.department_name like '%Finance');

--34. all the information of an employee whose salary and reporting person id is 3000 and 121 respectively. 
SELECT E.EMPLOYEE_ID , e.first_name, e.last_name, E.EMAIL, E.PHONE_NUMBER, E.HIRE_DATE, e.job_id,  e.SALARY, E.COMMISSION_PCT,E.MANAGER_ID,E.DEPARTMENT_ID
       FROM EMPLOYEES E WHERE (E.SALARY = 3000 AND E.EMPLOYEE_ID = 121);


--35. all the information of an employee whose id is any of the number 134, 159 and 183.
SELECT E.EMPLOYEE_ID , e.first_name, e.last_name, E.EMAIL, E.PHONE_NUMBER, E.HIRE_DATE, e.job_id,  e.SALARY, E.COMMISSION_PCT,E.MANAGER_ID,E.DEPARTMENT_ID
       FROM EMPLOYEES E WHERE ( E.EMPLOYEE_ID IN (134,159,183));

--36. all the information of the employees whose salary is within the range 1000 and 3000. 

SELECT E.EMPLOYEE_ID , e.first_name, e.last_name, E.EMAIL, E.PHONE_NUMBER, E.HIRE_DATE, e.job_id,  e.SALARY, E.COMMISSION_PCT,E.MANAGER_ID,E.DEPARTMENT_ID
       FROM EMPLOYEES E WHERE ( E.SALARY BETWEEN 1000 AND 3000);

--37. all the information of the employees whose salary is within the range of smallest salary and 2500. 
SELECT E.EMPLOYEE_ID , e.first_name, e.last_name, E.EMAIL, E.PHONE_NUMBER, E.HIRE_DATE, e.job_id,  e.SALARY, E.COMMISSION_PCT,E.MANAGER_ID,E.DEPARTMENT_ID
       FROM EMPLOYEES E WHERE ( E.SALARY BETWEEN  (SELECT MIN(E.SALARY) FROM EMPLOYEES)   AND 2500);

--38. all the information of the employees who does not work in those departments where some employees works whose id within the range 100 and 200. 
SELECT E.EMPLOYEE_ID , e.first_name, e.last_name, E.EMAIL, E.PHONE_NUMBER, E.HIRE_DATE, e.job_id,  e.SALARY, E.COMMISSION_PCT,E.MANAGER_ID,E.DEPARTMENT_ID
       FROM EMPLOYEES E WHERE (E.DEPARTMENT_ID NOT IN(SELECT E.DEPARTMENT_ID FROM EMPLOYEES WHERE E.EMPLOYEE_ID BETWEEN 100 AND 200));
       
--39. all the information for those employees whose id is any id who earn the second highest salary.

SELECT *
       FROM EMPLOYEES E WHERE (e.salary = (select max(E.SALARY)
               from employees e where e.salary < (
                       select max(e.salary) from employees e)));
       
--40. the employee name( first name and last name ) and hiredate for all employees in the same department as Clara. Exclude Clara. 

select E.first_name, E.last_name, E.HIRE_DATE
         FROM EMPLOYEES E  LEFT join departments D on (E.DEPARTMENT_ID = D.DEPARTMENT_ID  )
                      WHERE( D.DEPARTMENT_ID = ( SELECT D.DEPARTMENT_ID FROM EMPLOYEES E WHERE E.FIRST_NAME = 'Clara') AND e.FIRST_NAME != 'Clara')
                      order by e.first_name;
 
--41. the employee number and name( first name and last name ) for all employees who work in a department with any employee whose name contains a T. 
    select e.employee_id, E.first_name, E.last_name
           FROM EMPLOYEES E  join departments D on (E.DEPARTMENT_ID = D.DEPARTMENT_ID  )
                      WHERE( D.DEPARTMENT_ID in ( SELECT D.DEPARTMENT_ID FROM EMPLOYEES E WHERE E.FIRST_NAME like '%T%' or E.FIRST_NAME like '%T'or E.FIRST_NAME like 'T%' ) )
                      order by e.employee_id;
                      
--42. the employee number, name( first name and last name ), and salary for all employees who earn more than the average salary and who work in a department with any employee with a J in their name. 
select e.employee_id, E.first_name, E.last_name, E.SALARY
    FROM EMPLOYEES E  join departments D on (E.DEPARTMENT_ID = D.DEPARTMENT_ID  )
                      WHERE( E.SALARY > (SELECT AVG(SALARY) FROM EMPLOYEES)
                            AND D.DEPARTMENT_ID IN (SELECT D.DEPARTMENT_ID FROM EMPLOYEES E
                                                           WHERE E.FIRST_NAME like '%J%' or E.FIRST_NAME like '%J'or E.FIRST_NAME like 'J%' ) )
                                                           order by e.employee_id;

--43. the employee name( first name and last name ), employee id, and job title for all employees whose department location is Toronto. 
select  E.first_name, E.last_name,e.employee_id, J.JOB_TITLE
        FROM EMPLOYEES E JOIN DEPARTMENTS D ON(e.department_id = D.DEPARTMENT_ID)
                         JOIN JOBS J ON(E.JOB_ID = J.JOB_ID)
                         JOIN LOCATIONS L ON(D.LOCATION_ID = L.LOCATION_ID)
                              WHERE(L.CITY = 'Toronto');

--44. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN.  
     select  E.first_name, E.last_name,e.employee_id, J.JOB_TITLE
        FROM EMPLOYEES E JOIN JOBS J ON(E.JOB_ID = J.JOB_ID)
                              WHERE(e.salary < (SELECT max(E.SALARY) FROM EMPLOYEES E WHERE e.JOB_ID in 'MK_MAN'));

--45. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN. Exclude Job title MK_MAN.  
 
  select  E.first_name, E.last_name,e.employee_id, J.JOB_TITLE
        FROM EMPLOYEES E JOIN JOBS J ON(E.JOB_ID = J.JOB_ID)
                              WHERE(e.salary < (SELECT max(E.SALARY) FROM EMPLOYEES E WHERE e.JOB_ID in 'MK_MAN' )AND E.JOB_ID != 'MK_MAN')
                                    ORDER BY J.JOB_TITLE;
--46. all the information of those employees who did not have any job in the past. 

SELECT E.EMPLOYEE_ID , e.first_name, e.last_name, E.EMAIL, E.PHONE_NUMBER, E.HIRE_DATE, e.job_id,  e.SALARY, E.COMMISSION_PCT,E.MANAGER_ID,E.DEPARTMENT_ID
       FROM EMPLOYEES E LEFT JOIN JOB_HISTORY JH ON (JH.EMPLOYEE_ID = E.EMPLOYEE_ID)
             WHERE ( JH.EMPLOYEE_ID IS NULL);

--47. the employee number, name( first name and last name ) and job title for all employees whose salary is more than any average salary of any department.

SELECT E.employee_id, (E.FIRST_NAME || E.LAST_NAME) AS NAME, j.job_title
       from employees e join jobs j on (e.job_id = j.job_id)
            where(e.salary > (select min((J.MIN_SALARY+J.MAX_SALARY)/2) from JOBS J));

--48. the employee name( first name and last name ) and department for all employees for any existence of those employees whose salary is more than 3700.

SELECT (E.FIRST_NAME || ' '|| E.LAST_NAME) AS NAME, E.DEPARTMENT_ID
       FROM EMPLOYEES E
             WHERE ( salary > 3700);

--49. the department id and the total salary for those departments which contains at least one salaried employee.

select department_id , sum(salary) as total_salary 
       from employees
            group by department_id
            order by department_id;
            
--50. the employee id, name ( first name and last name ) and the job id column with a modified title SALESMAN for those employees whose job title is ST_MAN and DEVELOPER for whose job title is IT_PROG.  

SELECT EMPLOYEE_ID,
       (E.FIRST_NAME || ' '|| E.LAST_NAME) AS NAME ,
       CASE E.JOB_ID
          WHEN 'ST_MAN' THEN 'SALEMAN'
          WHEN 'IT_PROG' THEN 'DEVELOPER'
          ELSE E.JOB_ID
          END
FROM EMPLOYEES E LEFT JOIN JOBS J ON E.JOB_ID=J.JOB_ID

--51. the employee id, name ( first name and last name ), salary and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than the average salary of all employees.  

SELECT employee_id, (first_name || ' ' || last_name) as name, salary,
              CASE
                   WHEN SALARY > (SELECT AVG(SALARY) FROM EMPLOYEES) THEN 'HIGH'
                   WHEN SALARY < (SELECT AVG(SALARY) FROM EMPLOYEES)  THEN  'LOW'
                   ELSE 'The quantity is under 30'
                   END as SALARY_STATUS
                 from employees e ;

--52. the employee id, name ( first name and last name ), SalaryDrawn, AvgCompare (salary - the average salary of all employees) and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than the average salary of all employees.

SELECT employee_id, (first_name || ' ' || last_name) as name, salary as SalaryDrawn, (salary -( SELECT AVG(SALARY) FROM EMPLOYEES)) as AvgCompare,
              CASE
                   WHEN SALARY > (SELECT AVG(SALARY) FROM EMPLOYEES) THEN 'HIGH'
                   WHEN SALARY < (SELECT AVG(SALARY) FROM EMPLOYEES)  THEN  'LOW'
                   ELSE 'The quantity is under 30'
                   END as SALARY_STATUS
                 from employees
                 order by SALARY desc ;

--53. a set of rows to find all departments that do actually have one or more employees assigned to them.   

SELECT *
      FROM  DEPARTMENTS d join DEPARTMENTS E ON(E.DEPARTMENT_ID = D.DEPARTMENT_ID)
                          WHERE ((SELECT COUNT(EMPLOYEE_ID)FROM EMPLOYEES )>=1  );

--54. all employees who work in departments located in the United Kingdom. 

      select E.employee_id, (e.first_name || ' ' || e.last_name) as name
             from employees e join departments d on(e.department_id = d.department_id)
                              join locations l on (d.location_id = l.location_id)
                              join countries c on (l.country_id = c.country_id)
                                           where(c.country_name = 'United Kingdom');

--55. all the employees who earn more than the average and who work in any of the IT departments.

 select E.employee_id, (e.first_name || ' ' || e.last_name) as name, e.job_id
             from employees e where( 
             (e.job_id like 'IT%') 
              and 
             (e.salary >(select avg(salary)from employees )));

--56. who earns more than Mr. Ozer.  

 select E.employee_id, (e.first_name || ' ' || e.last_name) as name, e.job_id
             from employees e where( 
             e.salary >(select salary from employees 
                        where last_name = 'Ozer'));

--57. which employees have a manager who works for a department based in the US.   

 select E.employee_id, (e.first_name || ' ' || e.last_name) as name, e.job_id
             from employees e join departments d on(e.department_id = d.department_id)
                              join locations l on (d.location_id = l.location_id)
                              where(e.manager_id  in (select e.manager_id from employees e where(l.country_id = 'US')));


--58. the names of all employees whose salary is greater than 50% of their departmentâ€™s total salary bill.  

 select E.employee_id, (e.first_name || ' ' || e.last_name) as name
        where (salary > (select sum(salary) from  )


--59. the details of employees who are managers.   

SELECT *
       FROM EMPLOYEES
       WHERE EMPLOYEE_ID IN(SELECT MANAGER_ID FROM EMPLOYEES);

--60. the details of employees who manage a department.   

SELECT *
       FROM EMPLOYEES e join departments d on (e.department_id = d.department_id)
       WHERE (e.employee_id in (select d.manager_id from departments d ));

--61. the employee id, name ( first name and last name ), salary, department name and city for all the employees who gets the salary as the salary earn by the employee which is maximum within the joining person January 1st, 2002 and December 31st, 2003.    

 select E.employee_id, (e.first_name || ' ' || e.last_name) as name, e.job_id, e.salary, e.department_id, l.city
        from employees e left join departments d on(e.department_id = d.department_id)
                           left join locations l on (d.location_id = l.location_id)
                             left join job_history jh on (JH.EMPLOYEE_ID = E.EMPLOYEE_ID and jh.department_id = d.department_id)
                                 where (e.salary = (select max(e.salary) from employees e where ( jh.start_date = date'2002-01-01' or  jh.start_date = date'2003-12-31')));
                                
--62. the department code and name for all departments which located in the city London. 

 select d.department_id , d.department_name
             from departments d  join locations l on (d.location_id = l.location_id)
                              where(l.city = 'London');

--63. the first and last name, salary, and department ID for all those employees who earn more than the average salary and arrange the list in descending order on salary. 

 select E.employee_id, (e.first_name || ' ' || e.last_name) as name, salary, e.department_id
        from employees e where( e.salary > ( select avg(salary) from employees ))
                                                          order by e.salary desc;

--64. the first and last name, salary, and department ID for those employees who earn more than the maximum salary of a department which ID is 40. 

 select E.employee_id, (e.first_name || ' ' || e.last_name) as name, salary, e.department_id
        from employees e where( e.salary > ( select max(salary) from employees where (department_id = 40  )))
                                                          order by e.salary desc;

--65. the department name and Id for all departments where they located, that Id is equal to the Id for the location where department number 30 is located. 

select d.department_id , d.department_name
             from departments d  join locations l on (d.location_id = l.location_id)
                              where(l.country_id in (select l.country_id from locations l 
                                                                        where(d.department_id = 30 )));

--66. the first and last name, salary, and department ID for all those employees who work in that department where the employee works who hold the ID 201. 

 select E.employee_id, (e.first_name || ' ' || e.last_name) as name, salary, e.department_id
        from employees e join departments d on(e.department_id = d.department_id)
        where( d.department_id in(select d.department_id from departments d where (e.employee_id = 201  )));
                                                         
--67. the first and last name, salary, and department ID for those employees whose salary is equal to the salary of the employee who works in that department which ID is 40. 

 select (e.first_name || ' ' || e.last_name) as name, salary, e.department_id
        from employees e 
        where( e.salary =( select e.salary from employees e where (e.employee_id = 40)) 
               and (e.department_id in (select d.department_id from departments d where (e.employee_id = 40))));

--68. the first and last name, and department code for all employees who work in the department Marketing.  

select (e.first_name || ' ' || e.last_name) as name,  e.department_id
        from employees e join departments d on(e.department_id = d.department_id)
              where ( d.department_name = 'Marketing');

--69. the first and last name, salary, and department ID for those employees who earn more than the minimum salary of a department which ID is 40. 

select (e.first_name || ' ' || e.last_name) as name,  e.department_id
        from employees e join departments d on(e.department_id = d.department_id)
              where ( e.salary > (select min(salary) from employees where( department_id = 40)));

--70. the full name,email, and designation for all those employees who was hired after the employee whose ID is 165. 

select (e.first_name || ' ' || e.last_name) as name, e.job_id
        from employees e left join job_history jh on (jh.employee_id = e.employee_id)
        where( jh.start_date >  ( select jh.start_date from job_history jh where (e.employee_id = 165))); 

--71. the first and last name, salary, and department ID for those employees who earn less than the minimum salary of a department which ID is 70. 

 select (e.first_name || ' ' || e.last_name) as name, salary, e.department_id
        from employees e join departments d on (e.department_id = d.department_id)
        where( e.salary <( select min(e.salary) from employees e where (d.department_id = 70))); 
               
--72. the first and last name, salary, and department ID for those employees who earn less than the average salary, and also work at the department where the employee Laura is working as a first name holder. 

select (e.first_name || ' ' || e.last_name) as name, salary, e.department_id
        from employees e join departments d on (e.department_id = d.department_id)
        where( e.salary <( select avg(e.salary) from employees e) ); 

--73. the city of the employee whose ID 134 and works there. 

select l.city 
       from employees e join departments d on(e.department_id = d. department_id)
                        join locations l on (d.location_id = l.location_id)
                        where (e.employee_id = 134);

--74. the the details of those departments which max salary is 7000 or above for those employees who already done one or more jobs.  

select DISTINCT D.* from departments d
         join employees e on(d.department_id = e.department_id)
          JOIN JOBS J ON (E.JOB_ID = J.JOB_ID)
         where (MAX_salary > 7000  AND E.JOB_ID = J.JOB_ID);

--75. the detail information of those departments which starting salary is at least 8000.  

select DISTINCT d.* from departments d
         join employees e on(d.department_id = e.department_id)
         JOIN JOBS J ON (E.JOB_ID = J.JOB_ID)
         where (J.MIN_SALARY >= 8000);

--76. the full name (first and last name) of manager who is supervising 4 or more employees.

SELECT first_name || ' ' || last_name AS Manager_name
	FROM employees
		WHERE employee_id IN
		      (SELECT manager_id
		       FROM employees
		       GROUP BY manager_id
			   HAVING COUNT(*)>=4);

--77. the details of the current job for those employees who worked as a Sales Representative in the past.

select j.* from employees e
                     left join jobs j on (e.job_id = j.job_id)
                     left join job_history jh on(jh.employee_id = e.employee_id and jh.job_id = j.job_id )
                                                 where (jh.job_id = (select j.job_id from jobs j where (j.job_title ='Sales Representative')));

--78. all the infromation about those employees who earn second lowest salary of all the employees.

SELECT *
       FROM EMPLOYEES  WHERE
                (salary = (select MIN(SALARY) from employees
                        where salary > (select MIN(salary) from employees)));

--79. the details of departments managed by Susan.  

select * from departments d join employees e on(d.department_id = e.department_id)
        where(d.manager_id in (select employee_id from employees where first_name = 'Susan'));

--80. the department ID, full name (first and last name), salary for those employees who is highest salary drawar in a department.

 SELECT department_id, first_name || ' ' || last_name AS name, salary
	    FROM employees a
		WHERE salary =
			 (SELECT MAX(salary)
			  FROM employees
			  WHERE department_id = a.department_id);

      
       
       
